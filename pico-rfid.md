# Raspberry Pi Pico Project: RFID Access Control System
I've been having a lot of fun playing around with the Pi Pico and a lot of the additional hardware that has either been made specifically for or will work well with the Pi Pico.

In this instance, I've got an RFID module from SB Components, which I'm using to make a simple access control system.

The main idea behind this system is that there is an RFID reader which takes ID cards and sees if they're in a white list of pre-authorised keys. If so, then a relay will toggle for about 5 seconds, allowing the hypothetical user to open the door.

```py
allowed_cards = [
    (b'DEADBEEF0000', "Alice"),
    (b'0000DEADBEEF', "Bob")
]

print("Waiting for RFID cards")
while True:
    if (rfid.any() > 11):
        id = rfid.read(12)
        match = False
        for x in allowed_cards:
            if x[0] == id:
                print("Matched Allowed Card: {} ({})".format(x[1], id.decode("UTF-8")))
                toggle_lock()
                match = True
                break
        if(not match):
            print("No Match. ID read: {}".format(id.decode("UTF-8")))
```

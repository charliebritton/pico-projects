# Pi Pico Voltage Sensor
This is another Pico project but includes some external hardware. This will be used to keep track of the voltage of the battery in my van and let me know if it gets too low.

The circuit is reasonably simple
```
+ --T--[ R1 ]--T-- +
    |          ^
Vin |          R2  Vout
    |          v
- --|----------|-- -
```

For a $V_{out}$ at a maximum of 3.3V and a $V_{in}$ that could range from 0-15V, I picked $R1 = 100k\Omega$ and $R2 = 28200 \Omega$, to satisfy the equation:
$$
V_{out} = V_{in} \times \frac{R_2}{R_1 + R_2}
$$

For $R_2$, I joined a 27K and 1.2K in series to get me to the desired value.

I then soldered these and checked with a multimeter on a 12V source, ensuring that the output did not go above 3.3V. I will calibrate the ADC aspect of the circuit in a bit.

The code for this is really quite simple, with most of it calculating the scaled value for the ADC such that it reads the voltage as opposed to the raw registers in the ADC.
```py
from machine import ADC
from utime import ticks_ms, sleep

batt_voltage = ADC(28)

def get_voltage():
    value_raw = batt_voltage.read_u16()
    y0 = 1103
    y1 = 18340
    x0 = 0
    x1 = 0.9174
    m = (y1-y0)/(x1-x0)

    scaled_v = (value_raw - y0) / m
    actual_v = scaled_v * (15/3.3)
    return actual_v if actual_v > 0 else 0

while True:
    print("{:.2f}".format(get_voltage()))
    sleep(0.2)
```

The logic behind the $y_0,y_1, x_0, x_1, m$ values is simply the $y = mx + c$ equation taught at school, which is then adapted such that we can solve for $x$:

$$
x = \frac{y-c}{m}
$$

And since $m$ can be calculated from $\Delta y / \Delta x$, which can be calculated with $y_1 - y_0 / x_1 - x_0$, with the readings of 0 and the ADC reading and then the voltage of the source connected as tested through the voltmeter and scaled by $3.3/15$, we can calculate the $x$ which is the scaled value and finally scale up with $15/3.3$ to get the true value.

The $V_{in}$ is connected to the 12V source and the $V_{out}$ is connected to one of the ADCs and ADC_GND on the Pico.

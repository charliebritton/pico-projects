# Pico Motion Detection with the HC-SR501 PIR Sensor
This is a cheap PIR sensor which takes 3 wires: 5V from VBUS, GND and a pin that toggles HI/LOW dependent on the motion detection.

Code for this one is really simple, just connect to any GPIO pin on the Pico and tune the potentiometers for desired effect.

```python
from machine import Pin
from time import sleep, ticks_ms

pir = Pin(27, Pin.IN, Pin.PULL_DOWN)

motion = False
time = ticks_ms()

while True:
    if(pir.value() == 1 and motion == False):
        delta = ticks_ms() - time
        print("Motion detected. Time delta: {}".format(delta))
        time = ticks_ms()
        motion = True
    elif(pir.value() == 0 and motion == True):
        delta = ticks_ms() - time
        print("End of motion detection. Time delta: {}".format(delta))
        time = ticks_ms()
        motion = False
```

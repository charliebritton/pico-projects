# Pi Pico Diesel Heater Controls
This part of the smart van project is to control the diesel heater without having to use the remote. The main reasons I want to do this are:

- ensure that the heater is switched off before I start driving (this should be done automatically)
- allow me to preheat the van remotely by using a shortcut or something
- automatically switch it off if I'm not in the van or something

I initially thought I'd be needing another piece of hardware but apparently my LoRaWAN HAT I bought can be setup as a 433MHz radio, which is the same frequency the heater uses.

I don't have an SDR or anything else capable of reading the output from the remote, which is what needs capturing so that I can then transmit from my chip, in addition to having the remote as a backup. Ideally, I'd attach directly to the controller but the interface is proprietary and that would probably require more hardware and wires.

I've found a library called [RadioLib](https://github.com/jgromes/RadioLib), which seems to support the SX1262 chip that I have.

```c
// SX1262 has the following connections:
// NSS pin:   LoRa_CS    which is Pin  5
// DIO1 pin:  DIO1       which is Pin 26
// NRST pin:  LoRa_RESET which is Pin 20
// BUSY pin:  LoRa_BUSY  which is Pin  4
SX1262 radio = new Module(5, 26, 20, 4);
```

Their example code seems to currently be failing on me but no idea why.
